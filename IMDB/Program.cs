﻿using IMDB.Domain;
using System;
using IMDB;
using System.Collections.Generic;
using IMDB.Repository;
using System.Text.RegularExpressions;
using System.Linq;

namespace IMDB
{
    class Program
    {


        static void Main(string[] args)
        {

            bool flag = true;


            var _actorService = new ActorService();
            var _producerService = new ProducerService();
            var _movieService = new MovieService(_actorService, _producerService);

            Console.WriteLine("What Do You Want To Do");
            Console.WriteLine("----------------------");
            Console.WriteLine("1 : => List Movies");
            Console.WriteLine("2 : => Add Movies");
            Console.WriteLine("3 : => Add Actors");
            Console.WriteLine("4 : => Add Producers");
            Console.WriteLine("5 : => Delete Movie");
            Console.WriteLine("6 : => Delete Producer");
            Console.WriteLine("7 : => Delete Actor");
            Console.WriteLine("8 : => Exit");
            while (flag)
            {
                try
                {
                    Console.WriteLine("\n What Do You Want To Do");
                    int choice = 0;
                    if (int.TryParse(Console.ReadLine(), out int parsedChoice) && parsedChoice > 0)
                    {
                        choice = parsedChoice;
                    }

                    Console.WriteLine();
                    switch (choice)
                    {
                        case 1:
                            _movieService.Display();
                            break;

                        case 2:
                            var actors = _actorService.GetAll();
                            var producers = _producerService.GetAll();
                            if (actors.Count == 0 && producers.Count == 0)
                            {
                                throw new Exception("Please add actor and producer first");
                            }
                            else if (actors.Count == 0)
                            {
                                throw new Exception("please add actor first");
                            }
                            else if (producers.Count == 0)
                            {
                                throw new Exception("please add actor first");
                            }
                            Console.WriteLine("Enter the Name of Movie");
                            var name = Console.ReadLine();
                            Console.WriteLine("Year of Release");
                            var releaseDate = Console.ReadLine();
                            Console.WriteLine("Plot of movie");
                            var plot = Console.ReadLine();
                            Console.WriteLine("Actor is \n");
                            foreach (var actor in actors)
                            {
                                Console.WriteLine(actor.Id + "  " + actor.Name);
                            }
                            var nums = Console.ReadLine();
                            Console.WriteLine(" Producer is \n");

                            foreach (var producer in producers)
                            {
                                Console.WriteLine(producer.Id + "  " + producer.Name);
                            }
                            var producerId = Console.ReadLine();
                            _movieService.AddMovie(name, releaseDate, plot, nums, producerId);
                            break;

                        case 3:
                            Console.WriteLine("Enter the Name Actor");
                            var ActorName = Console.ReadLine();
                            Console.WriteLine("Date of Birth(dd/mm/yyyy):");
                            var aBirth = Console.ReadLine();
                            Console.WriteLine("Enter the year of experience");
                            var experience = Console.ReadLine();
                            _actorService.AddActor(ActorName, aBirth, experience);
                            break;

                        case 4:
                            Console.WriteLine("Enter the Name Producer");
                            var ProducerName = Console.ReadLine();
                            Console.WriteLine("Date of Birth(dd/mm/yyyy):");
                            var pBirth = Console.ReadLine();
                            _producerService.AddProducer(ProducerName, pBirth);
                            break;

                        case 5:

                            var movies = _movieService.GetAll();
                            if(movies.Count == 0)
                            {
                                throw new Exception("Add movie first");
                            }
                            foreach (var movie in movies)
                            {
                                Console.WriteLine(movie.Id + "Name of Movie is " + movie.Title);
                            }
                            var movieNumb = Console.ReadLine();
                            _movieService.Delete(movieNumb);
                            break;

                        case 6:
                            var producerList = _producerService.GetAll();
                            if(producerList.Count == 0)
                            {
                                throw new Exception("Add producer first");
                            }
                            Console.WriteLine("Producer");
                            foreach (var prod in producerList)
                            {
                                Console.WriteLine(prod.Id + " " + prod.Name);
                            }
                            var producerNumb = Console.ReadLine();
                            _producerService.DeleteProducer(producerNumb);
                            break;

                        case 7:
                            var actorList = _actorService.GetAll();
                            if(actorList.Count == 0)
                            {
                                throw new Exception("Add actor first");
                            }
                            Console.WriteLine("Actors ");
                            foreach (var act in actorList)
                            {
                                Console.WriteLine(act.Id + " " + act.Name);
                            }
                            var actorNumb = Console.ReadLine();
                            _actorService.Delete(actorNumb);
                            break;
                        case 8:
                            flag = false;
                            break;

                        default:
                            Console.WriteLine("Enter a valid input\n");
                            break;

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
