﻿using IMDB.Domain;
using IMDB.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;

namespace IMDB
{
    public class ActorService
    {
        private readonly ActorRepo _actorRepo;

        public ActorService()
        {
            _actorRepo = new ActorRepo();
        }
        public void AddActor(string name, string birth, string experience)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("Invalid arguments");
            }
            DateTime aBirth;
            if (DateTime.TryParseExact(birth.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None, out DateTime dateTime))
            {
                aBirth = dateTime;//.ToString("dd/MM/yyyy");
            }
            else
            {
                throw new Exception("Date format not correct");
            }

            int actorexp = 0;
            if (int.TryParse(experience, out int parsedChoice) && parsedChoice >= 0)
            {
                actorexp = parsedChoice;
            }
            else
            {
                throw new Exception("Experience not entered properly");
            }
            var actor = new Actor()
            {
                Name = name,
                DateOfBirth = aBirth,
                Id = GetAll().Count + 1,
                Experience = actorexp
            };
            _actorRepo.Add(actor);
        }
        public List<Actor> GetAll()
        {
            return _actorRepo.GetActor();
          
        }

        public Actor GetEntity(int actorId)
        {
            if (actorId <= 0)
            {
                throw new ArgumentNullException(nameof(actorId));
            }
            var actor = GetAll().SingleOrDefault(p => p.Id == actorId);
            if (actor == null)
            {
                throw new Exception($"Actor not found for given id {actorId}");
            }
            return actor;
        }

        public void Delete(string Id)
        {
            if (int.TryParse(Id, out int parsedActorId) && parsedActorId > 0)
            {
                var actor = GetEntity(parsedActorId);
                _actorRepo.DeleteActor(actor);
            }
            else
            {
                throw new Exception("ActorId Invalid");
            }
        }
    }
}
