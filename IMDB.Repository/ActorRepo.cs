﻿using IMDB.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMDB.Repository
{
    public class ActorRepo
    {
        private List<Actor> _actors;

        public ActorRepo()
        {
            _actors = new List<Actor>();
        }
        public void Add(Actor actor)
        {
            _actors.Add(actor);
        }

        public List<Actor> GetActor()
        {
            return _actors.ToList();
        }

        public void DeleteActor(Actor actor)
        {
            _actors.Remove(actor);
        }


    }
}
