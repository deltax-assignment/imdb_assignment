﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
    public class Movie
    {
        public string Title { get; set; }

        public int YearOfRelease { get; set; }

        public string Plot { get; set; }

        public Producer Producer { get; set; }

        public List<Actor> Actors { get; set; }
        public int Id { get; set; }

        public Movie()
        {
            Actors = new List<Actor>();
        }
    }
}
